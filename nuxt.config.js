export default {
  // Global page headers: https://go.nuxtjs.dev/config-head


  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-seo'
  ],
  seo: {
    baseUrl: 'https://frostbutter.com',
    name: 'Frost Butter',
    templateTitle: '%name% — %title%',
    description: "Hi! I'm Nick, an indie developer.",
    keywords: 'indie, developer, nuxt',
    canonical: 'auto',
    isForcedTrailingSlash: false,
    author: 'Nick Frostbutter',
    twitter: {
      site: '@nickfrosty',
      creator: '@nickfrosty',
      card: 'summary',
    },

  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
